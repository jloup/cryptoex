package cryptoex

import (
	"database/sql/driver"
	"fmt"
	"strings"
)

//go:generate stringer -type=Currency,OrderSide,OrderType,OrderState -output const_string.go
type Currency uint32

const (
	UNKNOWN_CURRENCY Currency = iota
	ATOM
	BTC
	BCH
	BCHABC
	ETH
	LTC
	EUR
	USD
	USDT
	XRP
	XMR
	XTZ
	SVT
	IOV
	ALEPH
	LAST_CURRENCY_INDEX
)

func (c *Currency) UnmarshalBinary(b []byte) error {
	*c = CurrencyFromString(string(b))

	return nil
}

func (c Currency) MarshalBinary() ([]byte, error) {
	return []byte(c.String()), nil
}

func (c Currency) Value() (driver.Value, error) {
	return c.String(), nil
}

func (c *Currency) Scan(value interface{}) error {
	switch v := value.(type) {
	case string:
		*c = CurrencyFromString(v)
	case []byte:
		*c = CurrencyFromString(string(v))
	default:
		return fmt.Errorf("Could not convert value '%+v' to currency", value)
	}

	return nil
}

func (c Currency) MarshalJSON() ([]byte, error) {
	return []byte("\"" + c.String() + "\""), nil
}

func (c *Currency) UnmarshalJSON(b []byte) error {
	if len(b) > 2 && b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}

	if len(b) == 0 {
		return fmt.Errorf("YO")
	}

	*c = CurrencyFromString(string(b))
	return nil
}

func CurrencyFromString(s string) Currency {
	switch strings.ToLower(s) {
	case "btc":
		return BTC
	case "bch":
		return BCH
	case "bchabc":
		return BCHABC
	case "eth":
		return ETH
	case "ltc":
		return LTC
	case "eur":
		return EUR
	case "usd":
		return USD
	case "usdt":
		return USDT
	case "xrp":
		return XRP
	case "xmr":
		return XMR
	case "svt":
		return SVT
	case "atom":
		return ATOM
	case "xtz":
		return XTZ
	case "iov":
		return IOV
	case "aleph":
		return ALEPH
	}

	return UNKNOWN_CURRENCY
}

type OrderSide uint8

const (
	UNKNOWN_ORDER_SIDE OrderSide = iota
	SELL
	BUY
)

func OrderSideFromString(s string) OrderSide {
	switch strings.ToLower(s) {
	case "buy":
		return BUY
	case "sell":
		return SELL
	}

	return UNKNOWN_ORDER_SIDE
}

func (o *OrderSide) UnmarshalBinary(b []byte) error {
	*o = OrderSideFromString(string(b))

	return nil
}

func (o OrderSide) MarshalBinary() ([]byte, error) {
	return []byte(strings.ToLower(o.String())), nil
}

func (o OrderSide) Value() (driver.Value, error) {
	return strings.ToLower(o.String()), nil
}

func (o *OrderSide) Scan(value interface{}) error {
	switch v := value.(type) {
	case string:
		*o = OrderSideFromString(v)
	case []byte:
		*o = OrderSideFromString(string(v))
	default:
		return fmt.Errorf("Could not convert value '%+v' to order side", value)
	}

	return nil
}

func (o OrderSide) MarshalJSON() ([]byte, error) {
	return []byte("\"" + strings.ToLower(o.String()) + "\""), nil
}

func (o *OrderSide) UnmarshalJSON(b []byte) error {
	if len(b) > 2 && b[0] == '"' && b[len(b)-1] == '"' {
		b = b[1 : len(b)-1]
	}

	if len(b) == 0 {
		return fmt.Errorf("YO")
	}

	*o = OrderSideFromString(string(b))
	return nil
}

type OrderType uint8

const (
	MARKET OrderType = iota + 1
	LIMIT
)

type OrderState uint8

const (
	STATE_ALL OrderState = iota
	OPENED
	COMPLETED
	CANCELED
)
