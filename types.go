package cryptoex

import (
	"fmt"

	"github.com/shopspring/decimal"
)

// Market
type Market struct {
	BaseUnit  Currency
	QuoteUnit Currency
}

func (m Market) Name() string {
	return fmt.Sprintf("%s/%s", m.BaseUnit, m.QuoteUnit)
}

type MarketList [LAST_CURRENCY_INDEX][LAST_CURRENCY_INDEX]Market

func (m MarketList) GetMarket(base Currency, quote Currency) (Market, bool) {
	market := m[base][quote]
	if market.BaseUnit == UNKNOWN_CURRENCY || market.QuoteUnit == UNKNOWN_CURRENCY {
		return market, false
	}

	return market, true
}

func NewMarketList(markets []Market) MarketList {
	list := MarketList{}
	for _, market := range markets {
		list[market.BaseUnit][market.QuoteUnit] = market
	}

	return list
}

// Order
type Order struct {
	Market   Market
	Side     OrderSide
	Price    decimal.Decimal
	Quantity decimal.Decimal
}

func (o Order) Volume() decimal.Decimal {
	return o.Price.Mul(o.Quantity)
}

// Sortable Order collection
type Orders []Order

func (o Orders) Len() int {
	return len(o)
}

func (o Orders) Less(i, j int) bool {
	if o[i].Side == SELL {
		return o[i].Price.LessThan(o[j].Price)
	} else {
		return o[i].Price.GreaterThan(o[j].Price)
	}
}

func (o Orders) Swap(i, j int) {
	p := o[i].Price
	q := o[i].Quantity

	o[i].Price = o[j].Price
	o[i].Quantity = o[j].Quantity

	o[j].Price = p
	o[j].Quantity = q
}

type OrderRecord struct {
	Id    string
	State OrderState
	Order
}

// OrderBook
type OrderBook struct {
	Bids Orders
	Asks Orders
}

// Trade
type Trade struct {
	OrderId  string
	Market   Market
	Price    decimal.Decimal
	Quantity decimal.Decimal
}
