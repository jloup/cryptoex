package exchanges

import (
	"strings"
)

type ClientConfig struct {
	key    string
	secret string
	mode   ClientEnvironment
}

func NewKeySecretConfig(key, secret string) ClientConfig {
	return ClientConfig{
		key:    key,
		secret: secret,
		mode:   SANDBOX,
	}
}

func NewTokenConfig(token string) ClientConfig {
	return ClientConfig{
		key:  token,
		mode: SANDBOX,
	}
}

func NoAuthConfig() ClientConfig {
	return ClientConfig{}
}

func (c ClientConfig) GetKeyAndSecret() (string, string) {
	return c.key, c.secret
}

func (c ClientConfig) GetToken() string {
	return c.key
}

func (c *ClientConfig) SetEnvironment(mode string) {
	switch strings.ToLower(mode) {
	case "sandbox":
		c.mode = SANDBOX
	case "production":
		c.mode = PRODUCTION
	default:
		c.mode = UNKNOWN_CLIENT_MODE
	}
}

func (c ClientConfig) Environment() ClientEnvironment {
	return c.mode
}
