package binance

import (
	"fmt"

	binance "github.com/jloup/go-binance"
	"github.com/shopspring/decimal"
	"gitlab.com/jloup/cryptoex"
	"gitlab.com/jloup/cryptoex/exchanges"
)

func New(config exchanges.ClientConfig) (exchanges.Client, error) {
	key, secret := config.GetKeyAndSecret()

	hmacSigner := &binance.HmacSigner{
		Key: []byte(secret),
	}

	service := binance.NewAPIService(
		"https://www.binance.com",
		key,
		hmacSigner,
		nil, //	logger,
		nil,
	)

	client := Client{
		c: binance.NewBinance(service),
	}

	return &client, client.Ping()
}

type depthWs struct {
	channel chan *binance.DepthEvent
	done    chan struct{}
}

type Client struct {
	c        binance.Binance
	depthWss [cryptoex.LAST_CURRENCY_INDEX][cryptoex.LAST_CURRENCY_INDEX]depthWs
}

func (c Client) Ping() error {
	return c.c.Ping()
}

func (c Client) BuildSymbol(base cryptoex.Currency, quote cryptoex.Currency) string {
	return fmt.Sprintf("%s%s", base.String(), quote.String())
}

func (c Client) OrderBook(base cryptoex.Currency, quote cryptoex.Currency) (cryptoex.OrderBook, error) {
	book, err := c.c.OrderBook(binance.OrderBookRequest{Symbol: c.BuildSymbol(base, quote), Limit: 500})
	if err != nil {
		return cryptoex.OrderBook{}, err
	}

	return convertOrderBook(cryptoex.Market{base, quote}, *book), nil
}

func (c Client) Markets() (cryptoex.MarketList, error) {
	exchangeInfo, err := c.c.ExchangeInfoQuery()
	if err != nil {
		return cryptoex.MarketList{}, err
	}

	return cryptoex.NewMarketList(convertExchangeInfo(*exchangeInfo)), nil
}

func (c Client) NewOrder(order cryptoex.Order, typ cryptoex.OrderType) (string, error) {
	return "", fmt.Errorf("not implemented")
}

func (c Client) CancelOrder(order cryptoex.Order, id string) error {
	return fmt.Errorf("not implemented")
}

func (c Client) SpotPrices(base, quote cryptoex.Currency, qty decimal.Decimal) (decimal.Decimal, decimal.Decimal, error) {
	book, err := c.c.OrderBook(binance.OrderBookRequest{Symbol: c.BuildSymbol(base, quote), Limit: 10})
	if err != nil {
		return decimal.Zero, decimal.Zero, err
	}

	bidPrice := decimal.Zero
	askPrice := decimal.Zero

	amount := qty
	for _, order := range book.Bids {
		amount = amount.Sub(decimal.NewFromFloat(order.Quantity))
		if amount.IsNegative() || amount.IsZero() {
			bidPrice = decimal.NewFromFloat(order.Price)
			break
		}
	}

	for _, order := range book.Asks {
		amount = amount.Sub(decimal.NewFromFloat(order.Quantity))
		if amount.IsNegative() || amount.IsZero() {
			askPrice = decimal.NewFromFloat(order.Price)
			break
		}
	}

	if bidPrice.IsZero() || askPrice.IsZero() {
		return decimal.Zero, decimal.Zero, fmt.Errorf("market is not deep enough")
	}

	return bidPrice, askPrice, nil
}

func (c Client) Orders(market cryptoex.Market, state cryptoex.OrderState) ([]cryptoex.OrderRecord, error) {
	return nil, fmt.Errorf("not implemented")
}

func (c Client) Trades(market cryptoex.Market) ([]cryptoex.Trade, error) {
	return nil, fmt.Errorf("not implemented")
}

func (c Client) GetLastTradePrice(cryptoex.Market) (decimal.Decimal, error) {
	return decimal.Zero, fmt.Errorf("not implemented")
}

func (c Client) ClearOrders(cryptoex.Market) error {
	return fmt.Errorf("not implemented")
}

/*
func (c *Client) EnableOrderBookWebsocket(baseUnit, quoteUnit cryptoex.Currency) error {
	channel, done, err := c.c.DepthWebsocket(binance.DepthWebsocketRequest{
		Symbol: c.BuildSymbol(baseUnit, quoteUnit),
	})

	if err != nil {
		return err
	}

	c.depthWss[baseUnit][quoteUnit] = depthWs{
		channel: channel,
		done:    done,
	}

	return nil
}

func (c Client) ProcessOrderBookEvent(baseUnit, quoteUnit cryptoex.Currency) (OrderBookEvent, error) {
	ws := c.depthWss[baseUnit][quoteUnit]
	if ws.channel == nil {
		return OrderBookEvent{}, fmt.Errorf("No websocket has been enabled for %s/%s pair.", baseUnit.String(), quoteUnit.String())
	}

	select {
	case event := <-ws.channel:
		return OrderBookEvent{
			WSEvent: WSEvent{
				Time: event.Time,
			},
			OrderBook: convertBinanceOrderBook(event.OrderBook),
		}, nil

	case <-ws.done:
		return OrderBookEvent{}, fmt.Errorf("Websocket for %s/%s pair has been terminated", baseUnit.String(), quoteUnit.String())
	}
}
*/
