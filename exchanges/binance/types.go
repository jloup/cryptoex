package binance

import (
	binance "github.com/jloup/go-binance"
	"github.com/shopspring/decimal"
	"gitlab.com/jloup/cryptoex"
)

func convertExchangeInfo(e binance.ExchangeInfo) []cryptoex.Market {
	markets := make([]cryptoex.Market, len(e.Symbols))

	for i, symbol := range e.Symbols {
		markets[i] = cryptoex.Market{
			BaseUnit:  cryptoex.CurrencyFromString(symbol.BaseAsset),
			QuoteUnit: cryptoex.CurrencyFromString(symbol.QuoteAsset),
		}
	}

	return markets
}

func convertOrderBook(market cryptoex.Market, book binance.OrderBook) cryptoex.OrderBook {
	orderBook := cryptoex.OrderBook{
		Bids: make([]cryptoex.Order, len(book.Bids)),
		Asks: make([]cryptoex.Order, len(book.Asks)),
	}

	for i, bid := range book.Bids {
		orderBook.Bids[i].Side = cryptoex.BUY
		orderBook.Bids[i].Market = market
		orderBook.Bids[i].Price = decimal.NewFromFloat(bid.Price)
		orderBook.Bids[i].Quantity = decimal.NewFromFloat(bid.Quantity)
	}

	for i, ask := range book.Asks {
		orderBook.Asks[i].Side = cryptoex.SELL
		orderBook.Asks[i].Market = market
		orderBook.Asks[i].Price = decimal.NewFromFloat(ask.Price)
		orderBook.Asks[i].Quantity = decimal.NewFromFloat(ask.Quantity)
	}

	return orderBook
}
