package exchanges

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type HttpClient struct {
	Headers http.Header
	baseUrl string
	client  *http.Client
}

func NewHttpClient(baseUrl string, timeout time.Duration, headers http.Header) HttpClient {
	return HttpClient{
		baseUrl: baseUrl,
		Headers: headers,
		client:  &http.Client{Timeout: timeout},
	}
}

func (h HttpClient) call(method string, path string, query url.Values, body io.Reader) (*http.Response, error) {
	var u *url.URL
	var err error

	if query != nil {
		u, err = url.Parse(fmt.Sprintf("%s%s?%s", h.baseUrl, path, query.Encode()))
	} else {
		u, err = url.Parse(fmt.Sprintf("%s%s", h.baseUrl, path))
	}
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		return nil, err
	}

	request.Header = h.Headers

	response, err := h.client.Do(request)

	if err != nil {
		return response, err
	}

	if response.Status[0] != '2' {
		b, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, err
		}

		response.Body.Close()

		return nil, fmt.Errorf("%v: %s", response.StatusCode, string(b))
	}

	return response, nil
}

func (h HttpClient) Get(path string, params url.Values) error {
	_, err := h.call("GET", path, params, nil)

	return err
}

func (h HttpClient) PostJSON(path string, params interface{}, out interface{}) error {
	b, err := json.Marshal(params)
	if err != nil {
		return err
	}

	response, err := h.call("POST", path, nil, bytes.NewBuffer(b))
	if err != nil {
		return err
	}

	defer response.Body.Close()

	if out != nil {
		dec := json.NewDecoder(response.Body)

		return dec.Decode(out)
	}

	return nil
}

func (h HttpClient) GetJSON(path string, params url.Values, out interface{}) error {
	response, err := h.call("GET", path, params, nil)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	if out != nil {
		dec := json.NewDecoder(response.Body)

		return dec.Decode(out)
	}

	return nil
}
