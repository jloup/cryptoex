package exchanges

//go:generate stringer -type=ClientEnvironment -output const_string.go
type ClientEnvironment uint8

const (
	UNKNOWN_CLIENT_MODE ClientEnvironment = iota
	SANDBOX
	PRODUCTION
)
