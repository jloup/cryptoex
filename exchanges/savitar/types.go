package savitar

import (
	"encoding/json"
	"fmt"

	"github.com/shopspring/decimal"
	"gitlab.com/jloup/cryptoex"
)

type Market struct {
	Id      string `json:"id"`
	AskUnit string `json:"ask_unit"`
	BidUnit string `json:"bid_unit"`
}

func convertMarket(m Market) cryptoex.Market {
	return cryptoex.Market{
		BaseUnit:  cryptoex.CurrencyFromString(m.BidUnit),
		QuoteUnit: cryptoex.CurrencyFromString(m.AskUnit),
	}
}

type generateJwtRequest struct {
	JwtToken string `json:"jwt_token"`
	Kid      string `json:"kid"`
}

type generateJwtResponse struct {
	Token string `json:"token"`
}

type Order struct {
	Id     json.Number     `json:"id,omitempty"`
	Market string          `json:"market"`
	Side   string          `json:"side"`
	Volume decimal.Decimal `json:"volume"`
	Price  decimal.Decimal `json:"price"`
	Type   string          `json:"ord_type"`
	State  string          `json:"state"`
}

type Trade struct {
	Id      json.Number     `json:"id,omitempty"`
	Market  string          `json:"market"`
	Volume  decimal.Decimal `json:"volume"`
	Price   decimal.Decimal `json:"price"`
	OrderId json.Number     `json:"order_id"`
}

func convertOrderState(s string) cryptoex.OrderState {
	switch s {
	case "wait":
		return cryptoex.OPENED
	case "done":
		return cryptoex.COMPLETED
	case "cancel":
		return cryptoex.CANCELED
	}

	panic(fmt.Sprintf("unknown ord state %v", s))
}

func convertOrdType(t string) cryptoex.OrderType {
	switch t {
	case "limit":
		return cryptoex.LIMIT
	case "market":
		return cryptoex.MARKET
	}

	panic(fmt.Sprintf("unknown ord type %v", t))
}

func convertOrdSide(t string) cryptoex.OrderSide {
	switch t {
	case "sell":
		return cryptoex.SELL
	case "buy":
		return cryptoex.BUY
	}

	panic(fmt.Sprintf("unknown ord type %v", t))
}

type cancelOrderRequest struct {
	Id string `json:"id"`
}

type lastTradesResponse struct {
	Price decimal.Decimal `json:"price"`
}
