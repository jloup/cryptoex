package savitar

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/hex"
	"fmt"
	"math/big"
	"math/rand"
	"net/http"
	"net/url"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/jloup/cryptoex/exchanges"
)

const TOKEN_EXPIRATION_SECONDS = 180       // 30 minutes
const TOKEN_EXPIRES_ALLOWANCE_SECONDS = 60 // 60 seconds

type authClient struct {
	key            string
	secret         string
	client         exchanges.HttpClient
	sessionClient  exchanges.HttpClient
	token          string
	tokenExpiresAt int64
}

func newAuthClient(key, secret string, env exchanges.ClientEnvironment) authClient {
	var httpClient, sessionHttpClient exchanges.HttpClient

	if env == exchanges.SANDBOX {
		httpClient = exchanges.NewHttpClient(_API_URL_SANDBOX, 20*time.Second, http.Header{})
		sessionHttpClient = exchanges.NewHttpClient(_AUTH_API_URL_SANDBOX, 20*time.Second, nil)
	} else {
		httpClient = exchanges.NewHttpClient(_API_URL_PRODUCTION, 20*time.Second, http.Header{})
		sessionHttpClient = exchanges.NewHttpClient(_AUTH_API_URL_PRODUCTION, 20*time.Second, nil)
	}

	return authClient{
		key:           key,
		secret:        secret,
		client:        httpClient,
		sessionClient: sessionHttpClient,
	}
}

func (a *authClient) buildJwt() jwt.StandardClaims {
	id := make([]byte, 6)
	rand.Read(id)

	now := time.Now().UTC()

	return jwt.StandardClaims{
		ExpiresAt: now.Add(TOKEN_EXPIRATION_SECONDS * time.Second).Unix(),
		Id:        strings.ToUpper(hex.EncodeToString(id)),
		IssuedAt:  now.Unix(),
		Issuer:    "external",
		Subject:   "api_key_jwt",
	}
}

func (a *authClient) signJwt(secret string, claims jwt.StandardClaims) (string, error) {
	i, ok := new(big.Int).SetString(secret, 16)
	if !ok {
		return "", fmt.Errorf("secret is not valid")
	}

	key := &ecdsa.PrivateKey{
		PublicKey: ecdsa.PublicKey{elliptic.P256(), &big.Int{}, &big.Int{}},
		D:         i,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)

	return token.SignedString(key)
}

func (a *authClient) authCall() error {
	jwtPayload := a.buildJwt()

	token, err := a.signJwt(a.secret, jwtPayload)
	if err != nil {
		return err
	}

	params := generateJwtRequest{
		JwtToken: token,
		Kid:      a.key,
	}

	var response string

	err = a.sessionClient.PostJSON("/identity/sessions", params, &response)
	if err != nil {
		return err
	}

	a.token = response
	a.tokenExpiresAt = jwtPayload.ExpiresAt
	a.client.Headers.Set("Authorization", fmt.Sprintf("%s", a.token))

	return nil
}

func (a *authClient) isTokenExpired() bool {
	if a.token == "" {
		return true
	}

	now := time.Now().UTC().Unix()

	return now > (a.tokenExpiresAt - TOKEN_EXPIRES_ALLOWANCE_SECONDS)
}

func (a *authClient) ensureTokenIsValid() error {
	if a.isTokenExpired() {
		return a.authCall()
	}

	return nil
}

func (a *authClient) PostJSON(path string, params interface{}, out interface{}) error {
	err := a.ensureTokenIsValid()
	if err != nil {
		return err
	}

	return a.client.PostJSON(path, params, out)
}

func (a *authClient) GetJSON(path string, params url.Values, out interface{}) error {
	err := a.ensureTokenIsValid()
	if err != nil {
		return err
	}

	return a.client.GetJSON(path, params, out)
}
