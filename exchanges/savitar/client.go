package savitar

import (
	"fmt"
	"net/url"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"gitlab.com/jloup/cryptoex"
	"gitlab.com/jloup/cryptoex/exchanges"
)

const _API_URL_PRODUCTION = "https://api.savitar.io/api/v2"
const _API_URL_SANDBOX = "http://api.dev.savitar.io/api/v2"

const _AUTH_API_URL_PRODUCTION = "https://auth.savitar.io/api/v2"
const _AUTH_API_URL_SANDBOX = "http://auth.dev.savitar.io/api/v2"

func New(config exchanges.ClientConfig) (exchanges.Client, error) {
	key, secret := config.GetKeyAndSecret()

	var httpClient exchanges.HttpClient

	if config.Environment() == exchanges.SANDBOX {
		httpClient = exchanges.NewHttpClient(_API_URL_SANDBOX, 20*time.Second, nil)
	} else {
		httpClient = exchanges.NewHttpClient(_API_URL_PRODUCTION, 20*time.Second, nil)
	}

	client := Client{
		config:     config,
		client:     httpClient,
		authClient: newAuthClient(key, secret, config.Environment()),
	}

	return &client, client.Ping()
}

type Client struct {
	config     exchanges.ClientConfig
	client     exchanges.HttpClient
	authClient authClient
}

func (c *Client) Ping() error {
	err := c.client.Get("/timestamp", nil)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) Markets() (cryptoex.MarketList, error) {
	response := make([]Market, 0)

	err := c.client.GetJSON("/markets", nil, &response)
	if err != nil {
		return cryptoex.MarketList{}, err
	}

	markets := make([]cryptoex.Market, len(response))
	for i, m := range response {
		markets[i] = convertMarket(m)
	}

	return cryptoex.NewMarketList(markets), nil
}

func (c *Client) BuildSymbol(base cryptoex.Currency, quote cryptoex.Currency) string {
	return strings.ToLower(fmt.Sprintf("%s%s", base.String(), quote.String()))
}

func (c *Client) OrderBook(base, quote cryptoex.Currency) (cryptoex.OrderBook, error) {
	return cryptoex.OrderBook{}, fmt.Errorf("not implemented")
}

func (c *Client) NewOrder(order cryptoex.Order, typ cryptoex.OrderType) (string, error) {
	response := Order{}

	params := Order{
		Market: c.BuildSymbol(order.Market.BaseUnit, order.Market.QuoteUnit),
		Side:   strings.ToLower(order.Side.String()),
		Volume: order.Quantity,
		Price:  order.Price,
		Type:   strings.ToLower(typ.String()),
	}

	err := c.authClient.PostJSON("/orders", params, &response)

	return string(response.Id), err
}

func (c *Client) CancelOrder(order cryptoex.Order, id string) error {
	err := c.authClient.PostJSON("/order/delete", cancelOrderRequest{id}, nil)

	return err
}

func (c *Client) ClearOrders(market cryptoex.Market) error {
	err := c.authClient.PostJSON("/orders/clear", nil, nil)

	return err
}

func (c Client) Orders(m cryptoex.Market, state cryptoex.OrderState) ([]cryptoex.OrderRecord, error) {
	response := make([]Order, 0)

	params := url.Values{}
	params.Set("market", c.BuildSymbol(m.BaseUnit, m.QuoteUnit))
	params.Set("limit", "1000")
	if state != cryptoex.STATE_ALL {
		stateString := ""
		switch state {
		case cryptoex.OPENED:
			stateString = "wait"
		case cryptoex.COMPLETED:
			stateString = "done"
		case cryptoex.CANCELED:
			stateString = "cancel"
		}

		params.Set("state", stateString)
	}

	err := c.authClient.GetJSON("/orders", params, &response)
	if err != nil {
		return nil, err
	}

	orders := make([]cryptoex.OrderRecord, len(response))
	for i, order := range response {
		orders[i] = cryptoex.OrderRecord{
			Id:    string(order.Id),
			State: convertOrderState(order.State),
			Order: cryptoex.Order{
				Side:     convertOrdSide(order.Side),
				Price:    order.Price,
				Quantity: order.Volume,
				Market:   cryptoex.Market{cryptoex.CurrencyFromString(order.Market[:3]), cryptoex.CurrencyFromString(order.Market[3:])},
			},
		}
	}

	return orders, nil
}

func (c Client) Trades(m cryptoex.Market) ([]cryptoex.Trade, error) {
	response := make([]Trade, 0)

	params := url.Values{}
	params.Set("market", c.BuildSymbol(m.BaseUnit, m.QuoteUnit))
	params.Set("limit", "1000")

	err := c.authClient.GetJSON("/trades/my", params, &response)
	if err != nil {
		return nil, err
	}

	trades := make([]cryptoex.Trade, len(response))
	for i, trade := range response {
		trades[i] = cryptoex.Trade{
			OrderId:  string(trade.OrderId),
			Price:    trade.Price,
			Quantity: trade.Volume,
			Market:   cryptoex.Market{cryptoex.CurrencyFromString(trade.Market[:3]), cryptoex.CurrencyFromString(trade.Market[3:])},
		}
	}

	return trades, nil
}

func (c *Client) GetLastTradePrice(m cryptoex.Market) (decimal.Decimal, error) {
	params := url.Values{}

	params.Set("market", c.BuildSymbol(m.BaseUnit, m.QuoteUnit))

	resp := make([]lastTradesResponse, 0)

	err := c.client.GetJSON("/trades", params, &resp)
	if err != nil {
		return decimal.Zero, err
	}

	if len(resp) == 0 {
		return decimal.Zero, fmt.Errorf("no trades")
	}

	return resp[0].Price, nil
}

func (c Client) SpotPrices(base, quote cryptoex.Currency, qty decimal.Decimal) (decimal.Decimal, decimal.Decimal, error) {
	return decimal.Zero, decimal.Zero, fmt.Errorf("not implemented")
}
