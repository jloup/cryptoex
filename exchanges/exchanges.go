package exchanges

import (
	"github.com/shopspring/decimal"
	"gitlab.com/jloup/cryptoex"
)

// Exchange Client
type Client interface {
	BuildSymbol(base cryptoex.Currency, quote cryptoex.Currency) string

	Ping() error

	Markets() (cryptoex.MarketList, error)

	OrderBook(base, quote cryptoex.Currency) (cryptoex.OrderBook, error)
	SpotPrices(base, quote cryptoex.Currency, qty decimal.Decimal) (decimal.Decimal, decimal.Decimal, error)

	NewOrder(order cryptoex.Order, typ cryptoex.OrderType) (string, error)
	CancelOrder(order cryptoex.Order, id string) error
	ClearOrders(cryptoex.Market) error
	Orders(cryptoex.Market, cryptoex.OrderState) ([]cryptoex.OrderRecord, error)
	Trades(cryptoex.Market) ([]cryptoex.Trade, error)
	GetLastTradePrice(cryptoex.Market) (decimal.Decimal, error)
}
