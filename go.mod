module gitlab.com/jloup/cryptoex

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.8.0 // indirect
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/jloup/go-binance v0.0.4
	github.com/pkg/errors v0.8.1 // indirect
	github.com/shopspring/decimal v1.2.0
	golang.org/x/tools v0.0.0-20191114222411-4191b8cbba09 // indirect
)

go 1.13
